import react from 'react';

import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { register } from '../../services/authService';
import { error } from 'console';
import { AxiosResponse } from 'axios';

// Define Schema of validation with Yup
const registerSchema = Yup.object().shape(
    {
        name: Yup.string()
            .min(6, 'Username must have 6 letters minimum')
            .max(12, 'Username must have maximum 12 letters')
            .required('Username is required'),
        email: Yup.string()
            .email('Invalid Email Format')
            .required('Email is required'),
        password: Yup.string()
            .min(8, 'Password must have 8 characters minimum')
            .required('Password is required'),
        confirm: Yup.string()
            .oneOf([Yup.ref('password')], 'Passwords must match')
            // Yup.string().when("password", {    
            //     is: (value: string) => (value && value.length > 0 ? true : false),
            //     then: Yup.string().oneOf(
            //         [Yup.ref("password")], 'Password must match'
            //     )
            // })
            .required('You must confirm you password'),
        age: Yup.number()
            .min(10, 'Minim Age is 10 years old')
            .required('Age is required'),
    }
);

// Register Component
const RegisterForm = () => {
    
    // We define the initial values
    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirm: '',
        age: 0,
    }

    return (
        <div>
            <h4>Register as new user</h4>
                {/* Formik to encapsulate a Form */}
                <Formik 
                    initialValues = { initialValues }
                    validationSchema = { registerSchema }
                    onSubmit = { async(values) => {
                        register(values.name, values.email, values.password, values.age).then((response: AxiosResponse) => {
                            if(response.status === 201){
                                console.log('Register user correctly');
                                console.log(response.data);
                                alert('User registered correctly');
                            }else{
                                throw new Error('Error registering a new user')
                            }
                        }).catch((error) => console.error(`[REGISTER ERROR]: Something went wrong: ${error}`))
                    } }
                >

                    {
                        ({ values, touched, errors, isSubmitting, handleChange, handleBlur}) => (
                            <Form>

                                {/* Name Field */}
                                <label htmlFor='name' >Name</label>
                                <Field id='name' type='name' name='name' placeholder='Name' />

                                {/* Name Errors */}
                                {
                                    errors.name && touched.name && (
                                        <ErrorMessage name='name' component='div'></ErrorMessage>
                                    )
                                }

                                {/* Email Field */}
                                <label htmlFor='email' >Email</label>
                                <Field id='email' type='text' name='email' placeholder='name@domain.com' />

                                {/* Email Errors */}
                                {
                                    errors.email && touched.email && (
                                        <ErrorMessage name='email' component='div'></ErrorMessage>
                                    )
                                }

                                {/* Password Field */}
                                <label htmlFor='password' >Password</label>
                                <Field id='password' type='password' name='password' />

                                {/* Password Errors */}
                                {
                                    errors.password && touched.password && (
                                        <ErrorMessage name='password' component='div'></ErrorMessage>
                                    )
                                }

                                {/* Confirm Field */}
                                <label htmlFor='confirm' >Confirm password</label>
                                <Field id='confirm' type='password' name='confirm' />

                                {/* Confirm Errors */}
                                {
                                    errors.confirm && touched.confirm && (
                                        <ErrorMessage name='confirm' component='div'></ErrorMessage>
                                    )
                                }

                                {/* Age Field */}
                                <label htmlFor='age' >Age</label>
                                <Field id='age' type='number' name='age' />

                                {/* Age Errors */}
                                {
                                    errors.age && touched.age && (
                                        <ErrorMessage name='age' component='div'></ErrorMessage>
                                    )
                                }


                                {/* SUBMIT FORM */}
                                <button type='submit'>Register</button>

                                {/* Message if the form is submitting */}
                                {
                                    isSubmitting ? (<p>Checking New User...</p>) : null
                                }
                            </Form>
                        )
                    }
                
                </Formik>
        </div>
    )

}

export default RegisterForm;