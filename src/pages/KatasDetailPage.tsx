import { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
// React Router DOM Imports
import { useNavigate, useParams } from 'react-router-dom';
import { isClassStaticBlockDeclaration } from 'typescript';
import { Editor } from '../components/editor/Editor';
import { useSessionStorage } from '../hooks/useSessionStorage';

import { getKataByID } from '../services/katasService';
import { Kata } from '../utils/types/kata.type';

const exampleCode = `
(function someDemo() {
  var test = "Hello World!";
  console.log(test);
})();

return () => <App />;
`;

export const KatasDetailPage = () => {

    // Find id from params
    let { id } = useParams();
    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();
    const [kata, setkata] = useState<Kata|undefined>(undefined);
    const [showSolution, setShowSolution] = useState(false);

    useEffect(() => {
        if(!loggedIn){
            return navigate('/login');
        }else{
            if(id){
                getKataByID(loggedIn, id).then((response: AxiosResponse) => {
                    if(response.status === 200 && response.data){
                        let kataData: Kata = {
                            _id: response.data._id,
                            name: response.data.name,
                            description: response.data.description,
                            valoration: response.data.valoration,
                            level: response.data.level,
                            chances: response.data.chances,
                            user: response.data.user,
                            participants: response.data.participants,
                            solution: response.data.solution,
                            numvaloration: response.data.numvaloration
                        }
                        setkata(kataData);
                        //console.table(kataData);
                    }else{
                        throw new Error(`Error obtaining kata by ID: ${response.data}`)
                    }
                }).catch((error) => console.error(`[Get Kata by ID error] ${error}`))

            }else{
                return navigate('/katas');
            }
        }

    }, [loggedIn])

    return (
        <div>
            <h1>
                Katas Detail Page: { id }
            </h1>
            { kata ? 
                <div className='kata-data'>
                    <h2>{kata?.description}</h2>
                    <h3>Rating: {kata.valoration}/5</h3>
                    <button onClick={()=> setShowSolution(!showSolution)}>
                        {showSolution ? 'Hide Solution': 'Show Solution'}
                    </button>
                    { showSolution ?
                        <Editor language='jsx'>{kata?.solution}</Editor>
                        :
                        null
                    }
                </div>
            :
                <div>
                    <h5>kata not found</h5>
                </div>
            }
        </div>
    )
}