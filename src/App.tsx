import React from 'react';
import './App.css';

// React Router DOM Imports
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { AppRoutes } from './routes/Routes';
import { StickyFooter } from './components/dashboard/StickyFooter';

// import LoginForm from './forms/LoginForm';
// import RegisterForm from './forms/RegisterForm';

function App() {
  return (
    <div className="App">
      {/* Render Login Form */}
      {/* <LoginForm /> */}
      {/* <RegisterForm /> */}
      <Router>
        {/* <nav>
          <ul>
            <li>
              <Link to='/'>Home</Link>
            </li>
            <li>
              <Link to='/login'>Login</Link>
            </li>
            <li>
              <Link to='/register'>Register</Link>
            </li>
            <li>
              <Link to='/katas'>Katas</Link>
            </li>
          </ul>
        </nav> */}
        <AppRoutes />
        

      </Router>
      <StickyFooter />

    </div>
  );
}

export default App;
