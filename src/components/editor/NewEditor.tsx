import Editor from 'react-simple-code-editor';
import React, { Fragment, useState } from 'react';
import Highlight, { defaultProps, Language } from 'prism-react-renderer';
import theme from 'prism-react-renderer/themes/nightOwl';

// TODO: Edit this CodeSnipped
const codeSnipped =
    `
import axios from 'axios';
cons getUser = () => {
    return axios.get('https://randomuser.me/api');
}
`

const CodeLanguage: Language[] =
[
    "tsx",
    "javascript",
    "python",
    "sql"
]


// Define Styles for Editor
const styles: any = {
    root: {
        boxSizing: 'border-box',
        fonFamily: '"Dank Mono", "Fira Code", monospace',
        ...theme.plain
    }
}

// Highlight Component
const HighlightElement = (code: string, language:any) => (
    <Highlight {...defaultProps} theme={theme} code={code} language={language} >
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
            <Fragment>
                {tokens.map((line, i) => (
                    <div {...getLineProps({ line: line, key: i })}>
                        {line.map((token, key) =>
                            <span {...getTokenProps({ token, key })} />
                        )}
                    </div>
                ))}
            </Fragment>
        )}
    </Highlight>
);

export const NewEditor = () => {

    const [code, setCode] = useState(codeSnipped);
    const handleChange = (newCode: string) => {
        setCode(newCode);
    }

    const [languageSelected, setLanguageSelected] = useState(CodeLanguage[0])
    const handleLanguageChange = (newLanguage: any) => {
        setLanguageSelected(newLanguage);
    }
     

    return (
        <div>
            <select value={languageSelected} onChange={(e) => handleLanguageChange(e.target.value)}>
                {CodeLanguage.map((language,index)=>(
                    <option value={language} key={index}>{language}</option>
                ))}
            </select>
            <Editor
                value={code}
                lang={languageSelected}
                onValueChange={handleChange}
                highlight={code => HighlightElement(code, languageSelected)}
                padding={10}
                style={styles.root}
            >
            </Editor>
        </div>
    )
}

