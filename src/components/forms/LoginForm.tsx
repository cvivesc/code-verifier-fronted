import react from 'react';

import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { useNavigate } from 'react-router-dom';

import { login } from '../../services/authService';
import { error } from 'console';
import { AxiosResponse } from 'axios';

// Define Schema of validation with Yup
const loginSchema = Yup.object().shape(
    {
        email: Yup.string().email('Invalid Email Format').required('Email is required'),
        password: Yup.string().required('Password is required')
    }
);

// Login Component
const LoginForm = () => {

    // We define the initial credentials
    const initialCredentials = {
        email: '',
        password:''
    }

    let navigate = useNavigate();

    return (
        <div>
            <h4>Login Form</h4>
                {/* Formik to encapsulate a Form */}
                <Formik
                    initialValues= { initialCredentials }
                    validationSchema= { loginSchema }
                    onSubmit= { async(values) =>{
                        // await new Promise((response) => setTimeout(response, 2000));
                        // alert(JSON.stringify(values, null, 2));
                        // console.table(values);

                        login(values.email, values.password).then( async(response: AxiosResponse) => {
                            if(response.status === 200){
                                if(response.data.token){
                                    //alert(JSON.stringify(response.data, null, 2));
                                    //console.table(response.data);
                                    await sessionStorage.setItem('sessionJWTToken', response.data.token);
                                    navigate('/');
                                }else{
                                    throw new Error('Error obtaining login Token')
                                }
                            }else{
                                throw new Error('Invalid credentials')
                            }
                        }).catch((error) => console.error(`[LOGIN ERROR]: Something went wrong: ${error}`))
                    }}
                >
                    {
                        ({ values, touched, errors, isSubmitting, handleChange, handleBlur}) => (
                            <Form>

                                {/* Email Field */}
                                <label htmlFor='email' >Email</label>
                                <Field id='email' type='email' name='email' placeholder='name@domain.com' />

                                {/* Email Errors */}
                                {
                                    errors.email && touched.email && (
                                        <ErrorMessage name='email' component='div'></ErrorMessage>
                                    )
                                }

                                {/* Password Field */}
                                <label htmlFor='password' >Password</label>
                                <Field id='password' type='password' name='password' />

                                {/* Password Errors */}
                                {
                                    errors.password && touched.password && (
                                        <ErrorMessage name='password' component='div'></ErrorMessage>
                                    )
                                }

                                {/* SUBMIT FORM */}
                                <button type='submit'>Login</button>

                                {/* Message if the form is submitting */}
                                {
                                    isSubmitting ? (<p>Checking Credentials...</p>) : null
                                }
                            </Form>
                        )}
                </Formik>
        </div>
    )
}

export default LoginForm;