import { useState } from "react";

import { Dropzone, FileMosaic, ExtFile, FullScreen, ImagePreview, VideoPreview } from '@files-ui/react';



export const FilesUploader = () => {
    const [files, setFiles] = useState<ExtFile[]>([]);
    const updateFiles = (incommingFiles: ExtFile[]) => {
        setFiles(incommingFiles);
    };
    const removeFile = (id: string | number | undefined) => {
        setFiles(files.filter((x) => x.id !== id));
    };
    const [imageSrc, setImageSrc] = useState(undefined);
    const handleSee = (imageSource: any) => {
        setImageSrc(imageSource);
    };
    const [videoSrc, setVideoSrc] = useState<File | string | undefined>(undefined);
    const handleWatch = (videoSource: File | string | undefined) => {
        setVideoSrc(videoSource);
    };

    const handleStart = (filesToUpload: ExtFile[]) => {
        console.log("advanced demo start upload", filesToUpload);
    };
    const handleFinish = (uploadedFiles: ExtFile[]) => {
        console.log("advanced demo finish upload", uploadedFiles);
    };
    const [extFiles, setExtFiles] = useState<ExtFile[]>([]);
    const handleAbort = (id: string | number | undefined) => {
        setExtFiles(
            extFiles.map((ef) => {
                if (ef.id === id) {
                    return { ...ef, uploadStatus: "aborted" };
                } else return { ...ef };
            })
        );
    };
    const handleCancel = (id: string | number | undefined) => {
        setExtFiles(
            extFiles.map((ef) => {
                if (ef.id === id) {
                    return { ...ef, uploadStatus: undefined };
                } else return { ...ef };
            })
        );
    };
    return (
        <>
            <Dropzone
                label={"Files ui ❤️"}
                style={{ minWidth: "550px" }}
                onChange={updateFiles}
                value={files}
                maxFileSize={28 * 1024 * 1024}
                maxFiles={2}
                actionButtons={{ position: "after", uploadButton: {}, abortButton: {} }}
                onUploadStart={handleStart}
                onUploadFinish={handleFinish}
                uploadConfig={{
                    url: "https://localhost:8000/api/uploadFile",
                    method: "POST",
                    headers: {
                        Authorization:
                            "bearer HTIBI/IBYG/&GU&/GV%&G/&IC%&V/Ibi76bfh8g67gg68g67i6g7G&58768&/(&/(FR&G/&H%&/",
                    },
                    cleanOnUpload: true,
                }}
                fakeUpload
            >
                {files.map((file: ExtFile) => (
                    <FileMosaic 
                        key={file.id} 
                        {...file} 
                        onDelete={removeFile} 
                        onSee={handleSee} 
                        onWatch={handleWatch} 
                        onAbort={handleAbort} 
                        onCancel={handleCancel}
                        info={true} 
                        preview
                    />
                ))}
            </Dropzone>
            <FullScreen
                open={imageSrc !== undefined}
                onClose={() => setImageSrc(undefined)}
            >
                <ImagePreview src={imageSrc} />
            </FullScreen>
            <FullScreen
                open={videoSrc !== undefined}
                onClose={() => setVideoSrc(undefined)}
            >
                <VideoPreview src={videoSrc} autoPlay controls />
            </FullScreen>
        </>
        // <Dropzone onChange={updateFiles} value={files}>
        //     {files.map((file:ExtFile) => (
        //         <FileMosaic {...file} preview />
        //     ))}
        // </Dropzone>
    );
}