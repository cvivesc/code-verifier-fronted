import React from 'react';

// Theme personalization of Material UI
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';

// CSS & Drawer
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';

// NavBar
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import ToolBar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

// Material Grids & Box
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';

// Material Lists
import List from '@mui/material/List';

// Icons
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import LogoutIcon from '@mui/icons-material/Logout';
import NotificationsIcon from '@mui/icons-material/Notifications';

// List for the Menu
import { MenuItems } from './MenuItems';

import { useState } from 'react';
import { NewEditor } from '../editor/NewEditor';
import { TipTapEditor } from '../editor/TipTapEditor';
import { FilesUploader } from '../uploader/FilesUploader';

// Width for Drawer Menu
const drawerWidth: number = 240;

// Props for AppBar
interface AppBarProps extends MuiAppBarProps {
    open?: boolean
}

// APP Bar
const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => (
    {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        ... (open && {
            marginLeft: drawerWidth,
            width: `calc(100% - ${drawerWidth}px)`,
            transition: theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            }),
        }),
    }
));

// Drawer Menu
const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        '& .MuiDrawer-paper': {
            position: 'relative',
            whiteSpace: 'nowrap',
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            }),
            boxSizing: 'border-box',
            ... (!open && {
                overflowX: 'hidden',
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen
                }),
                width: theme.spacing(7),
                // Breakpoints to Media Queries of CSS in diferent display sizes (Responsive Design) 
                [theme.breakpoints.up('sm')]: {
                    width: theme.spacing(9)
                }
            })
        }
    })
);

// Define Theme
const myTheme = createTheme();

// DashboardContent
// TODO: Refractor with navigation components
export const Dashboard = () => {
    const [open, setOpen] = useState(true);

    // Show/Hide Drawer Menu
    const toggleDrawer = () => {
        setOpen(!open);
    }

    return (
        <ThemeProvider theme={myTheme}>

            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                {/* AppBar */}
                <AppBar position='absolute' open={open}>
                    {/* Toolbar -> Acciones */}
                    <ToolBar sx={{ pr: '24px' }}>
                        {/* ICON to toggle Drawer Menu */}
                        <IconButton
                            edge='start'
                            color='inherit'
                            aria-label='open drawer'
                            onClick={toggleDrawer}
                            sx={{
                                marginRight: '36px',
                                ...(open && {
                                    display: 'none'
                                })
                            }}
                        >
                            <MenuIcon />
                        </IconButton>
                        {/* Title of APP */}
                        <Typography component='h1' variant='h6' color='inherit' noWrap sx={{ flexGrow: 1 }}>
                            Code Verification Katas
                        </Typography>
                        {/* ICON to show Notifications */}
                        <IconButton color='inherit'>
                            <Badge badgeContent={10} color='secondary'>
                                <NotificationsIcon />
                            </Badge>
                        </IconButton>
                        {/* ICON to logout */}
                        <IconButton color='inherit'>
                            <LogoutIcon />
                        </IconButton>
                    </ToolBar>
                </AppBar>

                <Drawer variant='permanent' open={open}>
                    <ToolBar
                        sx={{
                            display: 'fles',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            px: [1],
                        }}
                    >
                        {/* ICON to HIDE Menu */}
                        <IconButton color='inherit' onClick={toggleDrawer}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </ToolBar>
                    <Divider />
                    {/* List of Menu Items */}
                    <List component='nav'>
                        {MenuItems}
                    </List>
                </Drawer>
                {/* Dasboard Content */}
                <Box
                    component='main'
                    sx={{
                        backgroundColor: (theme) =>
                            theme.palette.mode === 'light'
                                ? theme.palette.grey[100]
                                : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                    }}
                >
                    <ToolBar />
                    {/* Container with the content */}
                    {/* TODO: Change for the navigation content by URL and Stack of Routes */}
                    <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12} lg={12}>
                                <Paper sx={{
                                    p: 2,
                                    m: 1,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: 340,
                                }}>
                                    <FilesUploader />
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                    <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12} lg={12}>
                                <Paper sx={{
                                    p: 2,
                                    m: 1,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: 340,
                                }}>
                                    <TipTapEditor />
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                    <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12} lg={12}>
                                <Paper sx={{
                                    p: 2,
                                    m: 1,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: 340,
                                }}>
                                    <NewEditor />
                                </Paper>
                            </Grid>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    )
}