import React from 'react';

// Materia List Components
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'

// Material Icon Components
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import LoginIcon from '@mui/icons-material/Login';
import DashboardIcon from '@mui/icons-material/Dashboard';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';

export const MenuItems = (
    <React.Fragment>
        {/* Dashboard to Katas Button */}
        <ListItemButton>
            <ListItemIcon>
                <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Katas" />
        </ListItemButton>
        {/* Users */}
        <ListItemButton>
            <ListItemIcon>
                <PersonOutlineIcon />
            </ListItemIcon>
            <ListItemText primary="Users" />
        </ListItemButton>
        {/* Ranking */}
        <ListItemButton>
            <ListItemIcon>
                <MilitaryTechIcon />
            </ListItemIcon>
            <ListItemText primary="Ranking" />
        </ListItemButton>
    </React.Fragment>
)